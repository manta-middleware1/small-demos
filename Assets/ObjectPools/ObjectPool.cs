using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MantaMiddleware.Demos.ObjectPools
{
	public class ObjectPool<T> where T : MonoBehaviour
	{
		public Transform transformParent;
		public Action<T> onResetObject;

		public T prefab;
		private Stack<T> objectsPool = new Stack<T>();
		private HashSet<T> inUseObjects = new HashSet<T>();

		public ObjectPool(Transform transformParent, T prefab, Action<T> onResetObject)
		{
			this.transformParent = transformParent;
			this.prefab = prefab;
			this.onResetObject = onResetObject;
			if (onResetObject == null) this.onResetObject = (o) => { };
		}

		public T GetObjectFromPool()
		{
			T rtn;
			if (objectsPool.Count > 0)
			{
				rtn = objectsPool.Pop();
				inUseObjects.Add(rtn);
				rtn.gameObject.SetActive(true);
				onResetObject(rtn);
				return rtn;
			}
			GameObject spawned = GameObject.Instantiate(prefab.gameObject, transformParent);
			spawned.SetActive(true);
			rtn = spawned.GetComponent<T>();
			inUseObjects.Add(rtn);
			onResetObject(rtn);
			return rtn;
		}

		public void Release(T poolObj)
		{
			if (inUseObjects.Remove(poolObj))
			{
				objectsPool.Push(poolObj);
				poolObj.gameObject.SetActive(false);
			}
		}

		public void ReleaseAll()
		{
			foreach (var poolObj in inUseObjects)
			{
				poolObj.gameObject.SetActive(false);
				objectsPool.Push(poolObj);
			}
			inUseObjects.Clear();
		}
	}

}