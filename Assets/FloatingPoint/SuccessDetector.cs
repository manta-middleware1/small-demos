using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SuccessDetector : MonoBehaviour
{
    public Transform targetCube;
    public GameObject successDisplayer;

    public enum SuccessType { Vector3Equality, ZEquality, DistanceCheck, ClosestApproach, TriggerCollision, ZThreshold }
    public SuccessType successType;

	private void Update()
	{
		switch (successType)
		{
			case SuccessType.Vector3Equality: isSuccess = CheckFloatEquality(); break;
			case SuccessType.ZEquality: isSuccess = CheckZEquality(); break;
			case SuccessType.DistanceCheck: isSuccess = CheckDistance(); break;
			case SuccessType.ClosestApproach: isSuccess = CheckClosestApproach(); break;
			case SuccessType.ZThreshold: isSuccess = CheckZThreshold(); break;
		}
	}


	bool CheckFloatEquality()
	{
		return (transform.position == targetCube.position);
	}

	bool CheckZEquality()
	{
		return (transform.position.z == targetCube.position.z);
	}









    public float distCheck = 0.3f;
	bool CheckDistance()
	{
		return (Vector3.Distance(transform.position, targetCube.position) < distCheck);
	}


	private float lastDistance = 999f;
	private bool wasApproaching = false;
	bool CheckClosestApproach() {
		float thisDist = Vector3.Distance(transform.position, targetCube.position);
		bool isApproaching = thisDist < lastDistance;

		if (thisDist < distCheck && !isApproaching && wasApproaching)
		{
			lastDistance = thisDist;
			wasApproaching = isApproaching;
			return true;
		}
		else
		{
			lastDistance = thisDist;
			wasApproaching = isApproaching;
			return false;
		}
	}



	bool CheckZThreshold()
	{
		return (transform.position.z < targetCube.position.z);
	}

	private bool isSuccess = false;

	// LIMIT ONCE CODE
	/*
	private bool wasSuccess = false;
	private void LateUpdate()
	{
		if (isSuccess && !wasSuccess)
		{
			successDisplayer.SetActive(true);
			Debug.Log("Success!");
		}
		if (!isSuccess && wasSuccess)
		{
			successDisplayer.SetActive(false);
		}

		wasSuccess = isSuccess;
	}*/
	
	// REGULAR CODE
	void LateUpdate()
    {
		if (isSuccess) {
			Debug.Log("Success!");
		}
		successDisplayer.SetActive(isSuccess);
    }
	

	private void OnTriggerEnter(Collider other)
	{
		if (successType == SuccessType.TriggerCollision)
		{
			if (other.transform == targetCube)
			{
				isSuccess = true;
			}
		}
	}
}
