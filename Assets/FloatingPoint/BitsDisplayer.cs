using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class BitsDisplayer : MonoBehaviour
{
	[SerializeField] private TMP_InputField inputField;
	[SerializeField] private TMP_Text outputField;
	private List<Image> bitImages = new List<Image>();
	public Color onColor = Color.green;
	public Color offColor = Color.black;

	private void Awake()
	{
		inputField.onValueChanged.AddListener(OnTextSubmit);
	}

	private void OnTextSubmit(string text)
	{
		Debug.Log($"Processing {text}");
		if (int.TryParse(text, out var number))
		{
			SetBits(BitConverter.GetBytes(number));
			outputField.text = $"Value as int: {number}";
		}
		else if (float.TryParse(text, out var fl)) {
			SetBits(BitConverter.GetBytes(fl));
			outputField.text = $"Value as float: {fl}";
		}
		else
		{
			outputField.text = "No parsed value.";
		}
	}

	public void SetBits(byte[] bits)
	{
		while (bitImages.Count < bits.Length * 8)
		{
			GameObject newBitGO = new GameObject("Bit");
			newBitGO.transform.parent = transform;
			bitImages.Add(newBitGO.AddComponent<Image>());
		}
		for (int b=0; b<bitImages.Count;b++)
		{
			if (b < bits.Length * 8)
			{
				bitImages[b].gameObject.SetActive(true);
				bitImages[b].transform.SetSiblingIndex(b);
				bitImages[b].color = GetBit(bits[b / 8], b % 8) ? onColor : offColor;
			}
			else
			{
				bitImages[b].gameObject.SetActive(false);
			}
		}
	}

	private bool GetBit(byte value, int bitIndex)
	{
		return (value >> bitIndex & 1)>0;
	}
}
