﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class AxisInfoDisplay : MonoBehaviour
{
    public Vector3 euler;
    public TMP_Text display;

    void Update()
    {
        transform.localRotation = Quaternion.Euler(euler);
        if (display != null)
        {
            Quaternion rot = Quaternion.Euler(euler);
            display.text = $"E: {euler:#.00}\nQ: {rot:#.00}";
        }
    }
}
