using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class RotationCounter : MonoBehaviour
{
    public TMP_Text text;
    public float GetCurrentRotation()
    {
        return -Mathf.Atan2(transform.right.y, transform.right.x) * Mathf.Rad2Deg;
    }

    private float rotationDegreeCount = 0f;
    private float lastRotation = -1f;
    public float flashDuration = 60f;

    void Start()
    {
        lastRotation = GetCurrentRotation();
    }

    void Update()
    {
        float thisRotation = GetCurrentRotation();
        while (thisRotation < lastRotation - 180f) thisRotation += 360f;
        while (thisRotation > lastRotation + 180f) thisRotation -= 360f;
        rotationDegreeCount += thisRotation - lastRotation;
        lastRotation = thisRotation;

        float nextLowest360 = Mathf.Floor(rotationDegreeCount / 360f) * 360f;
        text.text = $"Total degrees rotated: {rotationDegreeCount:#.00}";
        text.color = Color.Lerp(Color.green, Color.white, (rotationDegreeCount - nextLowest360) / flashDuration);
    }
}
