using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UISpawner : MonoBehaviour
{
    public static UISpawner Instance;

	public GameObject uiPrefab;

	public static void RegisterWalker(Walker walker)
	{
        Camera walkerCam = walker.GetComponentInChildren<Camera>();
        if (walkerCam != null)
		{
			GameObject spawnedUI = Instantiate(Instance.uiPrefab, Instance.transform);
			spawnedUI.GetComponent<WalkerUI>().SetTarget(walker);
		}
	}
	private void Awake()
	{
		Instance = this;
	}
}
