﻿using UnityEngine;

public class Walker : MonoBehaviour
{
	private void Start()
	{
		UISpawner.RegisterWalker(this);

		foreach (var ren in changeRendererColors)
		{
			if (ren is TrailRenderer tren)
			{
				tren.startColor = myColor;
				tren.endColor = myColor;
			}
			else
			{
				ren.material.color = myColor;
			}
		}
	}
	public float moveSpeed = 10f;
	public float rotSpeed = 10f;
	public Vector3 totalMovement = Vector3.zero;

	public Renderer[] changeRendererColors;
	public Color myColor = Color.white;
}
