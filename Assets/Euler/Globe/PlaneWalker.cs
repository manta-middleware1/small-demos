using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class PlaneWalker : Walker
{

	private void Update()
	{
		Vector3 rotDelta = new Vector3(0f, FakeInput.Instance.GetInput("Horizontal") * Time.deltaTime * rotSpeed, 0f);
		transform.Rotate(rotDelta);
		Vector3 posDelta = new Vector3(0f, 0f, FakeInput.Instance.GetInput("Vertical") * Time.deltaTime * moveSpeed);
		transform.Translate(posDelta);
		totalMovement += new Vector3(posDelta.z, rotDelta.y, 0f);
	}
}
