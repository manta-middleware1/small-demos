using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FakeInput : MonoBehaviour
{
    public static FakeInput Instance;

	public Walker watched;
	public bool isFaked = false;
	public TimedInstruction[] script;
	private int currentScriptStep = -1;
	private float currentScriptDistRemaining = 0f;
	private string currentAxis = "";
	//public float stepInterval = 0.2f;
	private bool globeVis = true;

	private void Awake()
	{
		Instance = this;
	}

	private void Start()
	{
		AdvanceStep();
		ToggleGlobeVis();
	}

	private void ToggleGlobeVis()
	{
		globeVis = !globeVis;
		var gws = FindObjectsOfType<GlobeWalker>();
		foreach (var gw in gws)
		{
			gw.GetComponentInChildren<Camera>().enabled = globeVis;
		}
	}

	private void AdvanceStep()
	{
		Debug.Log($"Advancing from step {currentScriptStep} to {currentScriptStep + 1}");
		currentScriptStep++;
		if (currentScriptStep < script.Length)
		{
			currentScriptDistRemaining = script[currentScriptStep].targetValue;
			currentAxis = script[currentScriptStep].axis;
		}
	}
	private float lastMovedTime = -99f;

	private void Update()
	{
		if (Input.GetKeyDown(KeyCode.Space)) isFaked = !isFaked;
		if (Input.GetKeyDown(KeyCode.H)) ToggleGlobeVis();
	}

	public float GetInput(string inputAxis)
	{
		if (!isFaked || currentScriptStep >= script.Length) return Input.GetAxis(inputAxis);
		if (inputAxis == currentAxis)
		{
			float thisDelta = Time.deltaTime * (inputAxis == "Vertical" ? watched.moveSpeed : watched.rotSpeed);
			if (currentScriptDistRemaining > 0f)
			{
				if (Time.time > lastMovedTime) currentScriptDistRemaining -= thisDelta;
				lastMovedTime = Time.time;
				if (currentScriptDistRemaining < 0f)
				{
					AdvanceStep();
				}
				return 1f;
			}
			else if (currentScriptDistRemaining < 0f)
			{
				if (Time.time > lastMovedTime) currentScriptDistRemaining += thisDelta;
				lastMovedTime = Time.time;
				if (currentScriptDistRemaining > 0f)
				{
					AdvanceStep();
				}
				return -1f;
			}
		}
		return 0f;
	}

	[Serializable]
	public class TimedInstruction
	{
		public float targetValue;
		public string axis;
	}
}
