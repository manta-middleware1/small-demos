using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobeWalker : Walker
{
	private void Update()
	{
		Vector3 delta = new Vector3(FakeInput.Instance.GetInput("Vertical") * Time.deltaTime * moveSpeed, FakeInput.Instance.GetInput("Horizontal") * Time.deltaTime * rotSpeed, 0f);
		transform.Rotate(delta);
		totalMovement += delta;
	}
}
