using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseLook : MonoBehaviour
{
    public bool rotateAxesIndividually = false;
    public bool doTrackRotation = false;
    public bool doClampRotation = false;

    public Vector2 trackedRotation = Vector2.zero;
    public float clampValue = 45f;

    void Update()
    {
        if (!doTrackRotation)
        {
            if (rotateAxesIndividually)
            {
                transform.RotateAround(transform.position, Vector3.up, -Input.GetAxis("Mouse X"));
                transform.RotateAround(transform.position, transform.right, Input.GetAxis("Mouse Y"));
            }
            else
                transform.Rotate(Input.GetAxis("Mouse Y"), -Input.GetAxis("Mouse X"), 0f);
            if (doClampRotation)
			{
                transform.eulerAngles = new Vector3(Mathf.Clamp(transform.eulerAngles.x, -clampValue, clampValue), transform.eulerAngles.y, transform.eulerAngles.z);
			}
        }
        else
		{
            trackedRotation.y -= Input.GetAxis("Mouse X");
            trackedRotation.x += Input.GetAxis("Mouse Y");
            if (doClampRotation) trackedRotation.x = Mathf.Clamp(trackedRotation.x, -clampValue, clampValue);
            transform.rotation = Quaternion.Euler(trackedRotation.x, trackedRotation.y, 0f);
		}
    }



    public bool doCheckUprightWithVector = false;
    public bool IsCharacterUpright()
	{
        Transform target = transform;

        Quaternion someRotation = transform.rotation;
        Vector3 transformForward = someRotation * Vector3.forward;

        float angle = 40f;
        Vector3 targetDirection = new Vector3(Mathf.Cos(angle), 0f, Mathf.Sin(angle));
        return Vector3.Angle(transform.forward, targetDirection) < 20f;
	}
}
